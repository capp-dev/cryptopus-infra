# Installing OCI tool and kubectl

```
brew update
brew install kubernetes-cli
brew upgrade openssl
pip install cryptography --global-option=build_ext --global-option="-L/usr/local/opt/openssl/lib" --global-option="-I/usr/local/opt/openssl/include"
export CPPFLAGS=-I/usr/local/opt/openssl/include
export LDFLAGS=-L/usr/local/opt/openssl/lib
bash -c "$(curl -L https://raw.githubusercontent.com/oracle/oci-cli/master/scripts/install/install.sh)"

source <(kubectl completion zsh)  # setup autocomplete in zsh into the current shell
echo "if [ $commands[kubectl] ]; then source <(kubectl completion zsh); fi" >> ~/.zshrc # add autocomplete permanently to your zsh shell
```

# Creating namespaces and using them

https://kubernetes.io/docs/tasks/administer-cluster/namespaces-walkthrough/

```
kubectl config view
kubectl config set-context dev --namespace=cryptopus-development --cluster=[READ FROM VIEW] --user=[READ FROM VIEW]
kubectl config set-context prod --namespace=cryptopus-production --cluster=[READ FROM VIEW] --user=[READ FROM VIEW]
kubectl config set-context dev-svc --namespace=cryptopus-dev-services --cluster=[READ FROM VIEW] --user=[READ FROM VIEW]
```

# Admin role on cluster

https://docs.cloud.oracle.com/iaas/Content/ContEng/Concepts/contengaboutaccesscontrol.htm
```
kubectl create clusterrolebinding mrm_cluster_admin --clusterrole=cluster-admin --user=user-c2doolbme4d
```

# Installing traefic with helm
First create the values.yml file with the following:
```
rbac:
  enabled: true
dashboard:
  enabled: true
  domain: traefik.kubernetes.local
kubernetes:
  namespaces:
    - kube-system
```
Then run the installation
```
helm install --values values.yml stable/traefik --namespace kube-system
```
